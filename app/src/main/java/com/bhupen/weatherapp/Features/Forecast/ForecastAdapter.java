package com.bhupen.weatherapp.Features.Forecast;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bhupen.weatherapp.DataTypes.ForecastList;
import com.bhupen.weatherapp.R;
import com.bhupen.weatherapp.Shared.Utils.TemperatureFormatter;
import com.bhupen.weatherapp.Shared.Utils.UnixTime2StringFormatter;

import java.util.List;


/**
 * Created by Bhupen on 12/06/2017.
 */

public class ForecastAdapter extends RecyclerView.Adapter<ForecastViewHolder> {

    private List<ForecastList> forecastLists;
    private Context context;
    private UnixTime2StringFormatter formatter;

    public ForecastAdapter(Context context, List<ForecastList> forecastLists) {
        this.forecastLists = forecastLists;
        this.context = context;
        formatter = new UnixTime2StringFormatter(context);
    }

    @Override
    public ForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, null, false);
        return new ForecastViewHolder(row);
    }

    @Override
    public void onBindViewHolder(ForecastViewHolder holder, int position) {
        ForecastList list = forecastLists.get(position);

        holder.getName().setText((formatter.format(list.dt))+ "  ");
        holder.getTemp().setText(TemperatureFormatter.format(list.temp.day));
    }

    @Override
    public int getItemCount() {
        return forecastLists.size();
    }
}
