package com.bhupen.weatherapp.Features.Forecast;

import android.content.Context;

import com.bhupen.weatherapp.DataTypes.Forecast;
import com.bhupen.weatherapp.DataTypes.ForecastList;
import com.bhupen.weatherapp.Shared.API.ApiService;
import com.bhupen.weatherapp.WeatherApp;

import java.util.List;

import javax.inject.Inject;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Bhupen on 12/06/2017.
 */

public class ForecastPresenterImpl implements ForecastPresenterInterface {

    private ForecastViewInterface view;

    @Inject
    ApiService apiService;

    public ForecastPresenterImpl(Context context) {
        ((WeatherApp)context).getAppComponent().inject(this);
    }


    @Override
    public void setView(ForecastViewInterface view) {
        this.view = view;
    }

    @Override
    public void loadForecast() {

        apiService.fetchWeatherForecasts()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<Forecast>() {
                    @Override
                    public void onCompleted() {
                        view.showComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(Forecast forecast) {
                        List<ForecastList> forecastLists = forecast.list;
                        view.showForecast(forecastLists);
                    }
                });
    }
}
