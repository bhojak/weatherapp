package com.bhupen.weatherapp.Features.Forecast;

import com.bhupen.weatherapp.DataTypes.ForecastList;

import java.util.List;

/**
 * Created by Bhupen on 12/06/2017.
 */

public interface ForecastViewInterface {

    void showForecast(List<ForecastList> forecastLists);

    void showError(String message);

    void showComplete();
}
