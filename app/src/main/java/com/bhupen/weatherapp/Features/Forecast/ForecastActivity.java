package com.bhupen.weatherapp.Features.Forecast;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.bhupen.weatherapp.DataTypes.ForecastList;
import com.bhupen.weatherapp.R;
import com.bhupen.weatherapp.WeatherApp;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Created by Bhupen on 12/06/2017.
 */

public class ForecastActivity extends AppCompatActivity implements ForecastViewInterface {


    private RecyclerView forecastRecyclerView;

    private Context context;

    private LinearLayoutManager mLinearLayoutManager;

    @Inject
    ForecastPresenterImpl presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((WeatherApp)getApplication()).getAppComponent().inject(this);
        context = getApplicationContext();
        ButterKnife.bind(this);
        forecastRecyclerView = (RecyclerView) findViewById(R.id.weather_forecast_list);

        presenter.setView(this);
        presenter.loadForecast();

    }

    @Override
    public void showForecast(List<ForecastList> forecastLists) {
        mLinearLayoutManager = new LinearLayoutManager(this);
        forecastRecyclerView.setLayoutManager(mLinearLayoutManager);
        forecastRecyclerView.setAdapter(new ForecastAdapter(context, forecastLists));
        forecastRecyclerView.getAdapter().notifyDataSetChanged();

    }


    @Override
    public void showError(String message) {
        Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showComplete() {
        Toast.makeText(this, "Complete", Toast.LENGTH_SHORT).show();
    }
}
