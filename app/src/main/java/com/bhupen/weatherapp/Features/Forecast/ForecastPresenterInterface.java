package com.bhupen.weatherapp.Features.Forecast;

/**
 * Created by Bhupen on 12/06/2017.
 */

public interface ForecastPresenterInterface {

    void loadForecast();

    void setView(ForecastViewInterface view);
}
