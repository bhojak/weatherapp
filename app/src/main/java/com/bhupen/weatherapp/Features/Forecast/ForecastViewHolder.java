package com.bhupen.weatherapp.Features.Forecast;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.bhupen.weatherapp.R;

/**
 * Created by Bhupen on 12/06/2017.
 */

public class ForecastViewHolder extends RecyclerView.ViewHolder {


    private TextView name, temp;



    public ForecastViewHolder(View itemView) {
        super(itemView);

        name = (TextView) itemView.findViewById(R.id.description);
        temp = (TextView) itemView.findViewById(R.id.maximum_temperature);
    }

    public TextView getName() {
        return name;
    }

    public TextView getTemp() {
        return temp;
    }

}


