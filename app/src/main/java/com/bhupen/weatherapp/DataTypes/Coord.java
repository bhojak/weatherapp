package com.bhupen.weatherapp.DataTypes;

/**
 * Created by Bhupen on 12/06/2017.
 */

public class Coord {

    public Float lat;
    public Float lon;
}
