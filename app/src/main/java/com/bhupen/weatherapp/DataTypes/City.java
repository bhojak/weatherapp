package com.bhupen.weatherapp.DataTypes;

/**
 * Created by Bhupen on 12/06/2017.
 */

public class City {

    public Integer id;
    public String name;
    public Coord coord;
    public String country;
}
