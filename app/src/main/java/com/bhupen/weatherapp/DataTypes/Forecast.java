package com.bhupen.weatherapp.DataTypes;

/**
 * Created by Bhupen on 12/06/2017.
 */

public class Forecast {

    public String cod;
    public Float message;
    public Integer cnt;
    public java.util.List<ForecastList> list = null;
    public City city;
}
