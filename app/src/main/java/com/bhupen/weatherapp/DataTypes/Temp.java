package com.bhupen.weatherapp.DataTypes;

/**
 * Created by Bhupen on 13/06/2017.
 */

public class Temp {

    public Float day;
    public Float min;
    public Float max;
    public Float night;
    public Float eve;
    public Float morn;
}
