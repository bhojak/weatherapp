package com.bhupen.weatherapp.DataTypes;

/**
 * Created by Bhupen on 12/06/2017.
 */

public class Weather {

    public Integer id;
    public Main main;
    public String description;
    public String icon;
}
