package com.bhupen.weatherapp.DataTypes;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bhupen on 12/06/2017.
 */

public class ForecastList {
    @SerializedName("dt")
    public long dt;
    public Temp temp;
}
