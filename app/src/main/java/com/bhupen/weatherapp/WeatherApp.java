package com.bhupen.weatherapp;

import android.app.Application;

import com.bhupen.weatherapp.Shared.DI.Components.AppComponent;
import com.bhupen.weatherapp.Shared.DI.Components.DaggerAppComponent;
import com.bhupen.weatherapp.Shared.DI.Modules.AppModule;

/**
 * Created by Bhupen on 12/06/2017.
 */

public class WeatherApp extends Application {

    private AppComponent appComponent;

    public AppComponent getAppComponent() {
        return appComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = initDagger(this);
    }

    protected AppComponent initDagger(WeatherApp application) {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(application))
                .build();
    }
}
