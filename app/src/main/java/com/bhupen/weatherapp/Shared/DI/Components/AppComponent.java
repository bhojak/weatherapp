package com.bhupen.weatherapp.Shared.DI.Components;

import com.bhupen.weatherapp.Features.Forecast.ForecastActivity;
import com.bhupen.weatherapp.Features.Forecast.ForecastPresenterImpl;
import com.bhupen.weatherapp.Shared.DI.Modules.AppModule;
import com.bhupen.weatherapp.Shared.DI.Modules.NetModule;
import com.bhupen.weatherapp.Shared.DI.Modules.PresenterModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Bhupen on 15/06/2017.
 */

@Singleton
@Component(modules = {AppModule.class, PresenterModule.class, NetModule.class})
public interface AppComponent {

    void inject(ForecastActivity target);

    void inject(ForecastPresenterImpl target);

}
