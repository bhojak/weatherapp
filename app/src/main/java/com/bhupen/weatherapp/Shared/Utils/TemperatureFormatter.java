package com.bhupen.weatherapp.Shared.Utils;

/**
 * Created by Bhupen on 12/06/2017.
 */

public class TemperatureFormatter {

    public static String format(float temperature) {
        return String.valueOf(Math.round(temperature)) + "°";
    }
}
