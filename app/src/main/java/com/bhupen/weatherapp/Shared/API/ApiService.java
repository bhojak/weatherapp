package com.bhupen.weatherapp.Shared.API;

import com.bhupen.weatherapp.DataTypes.Forecast;
import com.bhupen.weatherapp.Shared.Utils.Constants;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by Bhupen on 12/06/2017.
 */

public interface ApiService {

    @GET("forecast/daily?q=London,uk&mode=json&units=metric&cnt=5&apikey=" + Constants.API_KEY)
    Observable<Forecast> fetchWeatherForecasts();

}
