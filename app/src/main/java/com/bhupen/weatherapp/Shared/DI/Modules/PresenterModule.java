package com.bhupen.weatherapp.Shared.DI.Modules;

import android.content.Context;

import com.bhupen.weatherapp.Features.Forecast.ForecastPresenterImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Bhupen on 15/06/2017.
 */

@Module
public class PresenterModule {

    @Provides
    @Singleton
    ForecastPresenterImpl provideForecastPresenterImpl(Context context) {
        return new ForecastPresenterImpl(context);
    }


}

